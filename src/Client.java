import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {

        Socket socketClient;
        final String IP = "localhost";
        final int PORT = 3000;

        PrintStream out;
        Scanner in;
        Scanner sc;

        try {

            socketClient = new Socket(IP, PORT);
            out = new PrintStream(socketClient.getOutputStream());
            in = new Scanner(socketClient.getInputStream());
            sc = new Scanner(System.in);

            System.out.println("Conexão estabelecida!");

            while (true) {

                System.out.println("Digite (1) para IMPAR ou (2) para PAR ou (0) para SAIR: ");
                int opcao = sc.nextInt();

                if (opcao == 1 || opcao == 2) {

                    out.println(opcao);

                    System.out.println("Informe um nro de 0 a 5: ");
                    int numero = sc.nextInt();
                    out.println(numero);

                    String resultado = in.nextLine();

                    System.out.println("*** PLACAR ***");
                    System.out.println("RESULTADO: " + resultado);

                } else if (opcao == 0) {
                    
                    sc.close();
                    in.close();
                    socketClient.close();
                }
            }

        } catch (Exception e) {
            System.out.println("Erro " + e.getMessage());
        }

    }
}
