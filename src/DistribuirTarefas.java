import java.io.PrintStream;
import java.net.Socket;
import java.util.Random;
import java.util.Scanner;

public class DistribuirTarefas extends Thread {

    // Variaveis
    private Socket socketClient;
    private int totalJogador;
    private int totalComputador;
    private int parImpar;
    private int selecao;
    private int nroJogador;
    Scanner sc;

    public DistribuirTarefas(Socket socket) {
        this.socketClient = socket;
    }

    @Override
    public void run() {

        try {
            System.out.println("Distribuindo tarefas " + socketClient);

            while (true) {
                
                sc = new Scanner(socketClient.getInputStream());
                selecao = sc.nextInt();

                if (selecao == 0) {

                    sc.close();
                    socketClient.close();

                } else {

                    selecao(selecao);
                    //imprimir();

                    PrintStream printStream = new PrintStream(socketClient.getOutputStream());
                    printStream.println(imprimir());
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    // Selecao
    private void selecao(int selecao) {

        try {
            sc = new Scanner(socketClient.getInputStream());
            nroJogador = sc.nextInt();

            if (nroJogador >= 0 && nroJogador < 6) {

                Random nroAleatorio = new Random();
                int resp = nroAleatorio.nextInt(6);
                System.out.println("Nro gerado automaticamente: " + resp);
                int aux = resp + nroJogador;

                calculo(aux);

                if (parImpar == selecao) {
                    ++totalJogador;
                    System.out.println(":) Você GANHOU!!!");
                } else {
                    ++totalComputador;
                    System.out.println(":( Você PERDEU!!!");
                }
            } else {
                System.out.println("O número deve ser entre 0 e 5!");
            }

        } catch (Exception e) {
            System.out.println("Erro ao selecionar PAR ou IMPAR " + e.getMessage());
        }
    }

    // Verifica se é PAR ou IMPAR
    private int calculo(int aux) {

        if (aux % 2 == 0) {
            System.out.println("PAR");
            return parImpar = 2;
        } else {
            System.out.println("IMPAR");
            return parImpar = 1;
        }
    }

    // Imprimir Resultado
    private String imprimir() {
        String resultado = "JOGADOR: " + totalJogador + " | COMPUTADOR: "+ totalComputador;

        //System.out.println("*** PLACAR ***");
       // System.out.println("JOGADOR: " + totalJogador);
       // System.out.println("MAQUINA: " + totalComputador);

        return resultado;
    }

}
