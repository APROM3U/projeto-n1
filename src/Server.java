import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) {

        final int PORT = 3000;
        try {

            System.out.println("Iniciando servidor na porta " + PORT + "...");

            ServerSocket servidor = new ServerSocket(PORT);

            while (true) {
                
                Socket socket = servidor.accept();
                System.out.println("Aceitando novo cliente na porta " + socket.getPort());

                DistribuirTarefas distribuir = new DistribuirTarefas(socket);
                distribuir.start();

            }

        } catch (Exception e) {
            System.out.println("Servidor esta desligado" + e.getMessage());
        }

    }
}